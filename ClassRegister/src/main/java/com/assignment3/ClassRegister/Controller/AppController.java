package com.assignment3.ClassRegister.Controller;


import com.assignment3.ClassRegister.Entity.*;
import com.assignment3.ClassRegister.Repository.*;
import com.assignment3.ClassRegister.Error.CattedraAlreadyExistsException;
import com.assignment3.ClassRegister.Service.CattedraService;
import com.assignment3.ClassRegister.Error.ClasseAlreadyExistsException;
import com.assignment3.ClassRegister.Service.ClasseService;
import com.assignment3.ClassRegister.Error.MateriaAlreadyExistsException;
import com.assignment3.ClassRegister.Service.MateriaService;
import com.assignment3.ClassRegister.Service.PersonaService;
import com.assignment3.ClassRegister.Error.RegistroAlreadyExistsException;
import com.assignment3.ClassRegister.Error.StudentAlreadyEnrolledException;
import com.assignment3.ClassRegister.Service.RegistroClasseService;
import com.assignment3.ClassRegister.Service.RegistroProfessoreService;
import com.assignment3.ClassRegister.Error.VerificaAlreadyExistsException;
import com.assignment3.ClassRegister.Error.CattedraDoesntExistException;
import com.assignment3.ClassRegister.Service.VerificaService;



import com.assignment3.ClassRegister.Error.UserAlreadyExistsException;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class AppController {
	@Autowired
	PersonaRepository personaRepository;
	@Autowired
	PersonaService personaService;
	@Autowired
	ClasseService classeService;
	@Autowired
	MateriaService materiaService;
	@Autowired
	RegistroClasseService registroClasseService;
	@Autowired
	RegistroProfessoreService registroProfessoreService;
	@Autowired
	CattedraService cattedraService;
	@Autowired
	VerificaService verificaService;
	
	@RequestMapping("/")
    public String home() {
      return "home";
    }
	
	@RequestMapping(value="/home", method =RequestMethod.GET)
	public ModelAndView display(Principal principal){
        ModelAndView mav = new ModelAndView();
        //Persona p = personaService.findByEmail(principal.getName());
        
        mav.addObject("name", principal.getName());
        return mav;
    }
	
	@GetMapping("/login")
    public ModelAndView login() {
		ModelAndView mav = new ModelAndView("login");
		mav.addObject("persona", new Persona());
		return mav;
    }
	
	@GetMapping("/registerStudent")
	public String showRegistrationForm(Model model) {
	    
		model.addAttribute("studente", new Studente());
	     
	    return "registerStudent";
	}

	@PostMapping("/registerStudent")
	public String processRegistrazioneStudente( @Valid @ModelAttribute("studente")  Studente studente, BindingResult bindingResult,RedirectAttributes redirectAttributes,HttpServletRequest request) throws ServletException  {
		if (bindingResult.hasErrors()) {
			return "registerStudent";
		}

		try {
			personaService.save(studente);
        } catch (UserAlreadyExistsException e){
            redirectAttributes.addFlashAttribute("errorMessage", "Registrazione non effettuata, l'utente è già presente.");
            return "redirect:/registerStudent";
        }
		redirectAttributes.addFlashAttribute("successMessage", "Registrazione effettuata con successo, effettua il login.");
	    return "redirect:/login";
	}
	
	@GetMapping("/registerProfessor")
	public String showRegistrationFormP(Model model) {
	    
		model.addAttribute("professore", new Professore());
	     
	    return "registerProfessor";
	}

	@PostMapping("/registerProfessor")
	public String processregisterP(@Valid @ModelAttribute("professore") Professore professore, BindingResult bindingResult,Model model,RedirectAttributes redirectAttributes,HttpServletRequest request) throws ServletException  {
		if (bindingResult.hasErrors()) {
			return "registerProfessor";
		}
		try {
			personaService.save(professore);
        } catch (UserAlreadyExistsException e){
            redirectAttributes.addFlashAttribute("errorMessage", "Registrazione non effettuata, l'utente è già presente.");
            return "redirect:/registerProfessor";
        }
		redirectAttributes.addFlashAttribute("successMessage", "Registrazione effettuata con successo, effettua il login.");
	    return "redirect:/login";
	}
	
	@GetMapping("/editAccount")
	public String showEditForm(Model model, Principal principal) {
	    
		Persona p = personaService.findByEmail(principal.getName());
		model.addAttribute("persona", p);
	     
	    return "editAccount";
	}

	@PostMapping("/editAccount")
	public String editAccountDetails(@Valid @ModelAttribute("persona") Persona persona, BindingResult bindingResult,Principal principal,Model model,RedirectAttributes redirectAttributes, HttpServletRequest request) throws ServletException {
		if (bindingResult.hasErrors()) {
			return "editAccount";
		}
		personaService.updatePersonaById(personaService.findByEmail(principal.getName()).getId(), persona);
		request.logout();
		request.login(persona.getEmail(), persona.getPassword());
    
		redirectAttributes.addFlashAttribute("successMessageEdit", "Modifiche effettuate con successo, effettua il login.");
	    return "redirect:/paginaPersonale";
	}
	
	@GetMapping("/deleteAccount")
	public String showDeleteForm(Model model) {
	    
		model.addAttribute("persona", new Persona());
	     
	    return "deleteAccount";
	}

	@PostMapping("/deleteAccount")
	public String deleteAccount( @ModelAttribute("persona") Persona persona, Principal principal,RedirectAttributes redirectAttributes, HttpServletRequest request) throws ServletException {
		Persona p = personaRepository.findByEmail(principal.getName());
		BCryptPasswordEncoder passencoder = new BCryptPasswordEncoder();
		if(passencoder.matches(persona.getPassword(), p.getPassword())) {
			personaService.deleteById(p.getId());
			redirectAttributes.addFlashAttribute("successMessageDelete", "Eliminazione effettuata con successo!");
			request.logout();
		    return "redirect:/login";
		}
        
		redirectAttributes.addFlashAttribute("errorMessageDelete", "Eliminazione non effettuata.");
	    return "redirect:/paginaPersonale";
	}
	
	
	@RequestMapping(value="/paginaPersonale",method = RequestMethod.GET)
	public ModelAndView showPaginaProfessore(Principal principal) {
	    ModelAndView mav = new ModelAndView();
	    System.out.println(principal.getName());
	    System.out.println(principal.getClass());
	    Persona p = personaService.findByEmail(principal.getName());
	    System.out.println(p.getRuolo());
	    if(p.getRuolo().equals("Professore")) {
	    	Professore prof = (Professore) personaService.findByEmail(principal.getName());
	    	mav.addObject("cattedre", prof.getCattedre());
	    } else {
	    	Studente stud = (Studente) personaService.findByEmail(principal.getName());
	    	mav.addObject("classe", stud.getClasse());
	    }
	    

	    System.out.println(p.getEmail());
	    mav.addObject("persona", p);
	     
	    return mav;
	}
		
	@RequestMapping(value="/azioni",method = RequestMethod.GET)
	public String showPaginaAzioni(Principal principal, Model model) {
	    Persona p = personaService.findByEmail(principal.getName());
	    model.addAttribute("persona", p);
	    if(p.getRuolo().equalsIgnoreCase("Professore")) 
	    	return "azioniProfessore";
	   return "azioniStudente";
	}
	
	
	// AZIONI CLASSE
	
	@GetMapping("/creaClasse")
	public String showClassForm(Model model) {
	    
		model.addAttribute("classe", new Classe());
	     
	    return "creaClasse";
	}

	@PostMapping("/creaClasse")
	public String registraClasse(@Valid @ModelAttribute("classe") Classe classe,BindingResult bindingResult,Model model,RedirectAttributes redirectAttributes,Principal principal) {
		if (bindingResult.hasErrors()) {
			return "creaClasse";
		}
		try {
			classeService.save(classe);
        } catch (ClasseAlreadyExistsException e){
            redirectAttributes.addFlashAttribute("errorMessage", "Registrazione non effettuata, la classe è già presente.");
            return "redirect:/creaClasse";
        } catch (Exception e) {
        	e.printStackTrace();
        }
		
		redirectAttributes.addFlashAttribute("successMessage", "Registrazione della classe effettuata con successo.");
	    return "redirect:/paginaPersonale";
	}
	
	
	@GetMapping("/iscrizioneClasse")
	public String showClassesChoices(Model model) {
		model.addAttribute("classe_scelta", new Classe());
		model.addAttribute("classi", classeService.findAll());
	    return "iscrizioneClasse";
	}

	@PostMapping("/iscrizioneClasse")
	public String iscrizioneClasse(@ModelAttribute("classe_scelta") Classe classe,Model model,RedirectAttributes redirectAttributes,Principal principal) {
		
		Studente stud = (Studente) personaService.findByEmail(principal.getName());
		try {
			personaService.updateClasseStudenteById(stud.getId(),classeService.findById(classe.getIdClasse()));
		
		} catch (StudentAlreadyEnrolledException e) {
			redirectAttributes.addFlashAttribute("errorMessageStudent", "Lo studente è già iscritto ad una classe.");
		    return "redirect:/paginaPersonale";

		}
		classeService.updateStudentiClasse(stud, classe.getIdClasse());
		redirectAttributes.addFlashAttribute("successMessageClasse", "Registrazione alla classe effettuata con successo.");
	    return "redirect:/paginaPersonale";
	}
	
	@GetMapping("/editClasse")
	public String showEditClassForm(Model model) {
	    model.addAttribute("classi",classeService.findAll());
		model.addAttribute("classe_scelta", new Classe());
	    return "editClasse";
	}
	
	
	@PostMapping("/editClasse")
	public String modificaClasse(@Valid @ModelAttribute("classe_scelta") Classe classe_scelta, BindingResult bindingResult,Model model,RedirectAttributes redirectAttributes,Principal principal) {
		
		if (bindingResult.hasErrors()) {
			return "editClasse";
		}
		try {
            classeService.updateClasse(classe_scelta.getIdClasse(),classe_scelta);
        } catch (ClasseAlreadyExistsException e){
            redirectAttributes.addFlashAttribute("errorMessage", "Modifica non effettuata, il nome della classe è già in uso.");
            return "redirect:/editClasse";
        } catch (Exception e) {
        	e.printStackTrace();
        }
		
		redirectAttributes.addFlashAttribute("successMessage", "Registrazione della classe effettuata con successo.");
	    return "redirect:/paginaPersonale";
	}
	
	@GetMapping("/deleteClasse")
	public String showDeleteClassForm(Model model) {
	    model.addAttribute("classi",classeService.findAll());
		model.addAttribute("classe_scelta", new Classe());
	    return "deleteClasse";
	}
	
	
	@PostMapping("/deleteClasse")
	public String eliminaClasse(@ModelAttribute("classe_scelta") Classe classe_scelta,Model model,RedirectAttributes redirectAttributes,Principal principal) {
            classeService.deleteClasseById(classe_scelta.getIdClasse());
        
		redirectAttributes.addFlashAttribute("successMessageDeleteClass", "Eliminazione della classe effettuata con successo.");
	    return "redirect:/paginaPersonale";
	}
	
	
	
	// AZIONI MATERIA
	
	
	@GetMapping("/aggiungiMateria")
	public String showMateriaForm(Model model,Principal principal) {
		Professore professore = (Professore) personaService.findByEmail(principal.getName());

		model.addAttribute("materia", new Materia());
	    return "aggiungiMateria";
	}
	
	@RequestMapping(path="/aggiungiMateria",method= RequestMethod.POST)
	public String creaMateria(@Valid @ModelAttribute("materia") Materia materia, BindingResult bindingResult, RedirectAttributes redirectAttributes,Principal principal) {
		if (bindingResult.hasErrors()) {
			return "aggiungiMateria";
		}
		try{
			materiaService.save(materia);
		} catch (MateriaAlreadyExistsException e){
            redirectAttributes.addFlashAttribute("errorMessageMateriaAlreadyExists", "Creazione materia non effettuata. Materia giÃ  presente.");
            return "redirect:/paginaPersonale";
		} 
		redirectAttributes.addFlashAttribute("successMessageMateriaAdded", "Aggiunzione materia effettuata con successo.");
		return "redirect:/paginaPersonale";
	}
	
	@GetMapping("/editMateria")
	public String showEditMateriaForm(Model model) {
	    model.addAttribute("materie",materiaService.findAll());
		model.addAttribute("materia_scelta", new Materia());
	    return "editMateria";
	}
	
	
	@PostMapping("/editMateria")
	public String modificaMateria(@Valid @ModelAttribute("Materia_scelta") Materia materia_scelta, BindingResult bindingResult,Model model,RedirectAttributes redirectAttributes,Principal principal) {
		
		if (bindingResult.hasErrors()) {
			return "editMateria";
		}
		try {
            materiaService.updateMateria(materia_scelta.getIdMateria(),materia_scelta);
        } catch (MateriaAlreadyExistsException e){
            redirectAttributes.addFlashAttribute("errorMessage", "Modifica non effettuata, il nome della materia è già in uso.");
            return "redirect:/editMateria";
        } catch (Exception e) {
        	e.printStackTrace();
        }
		
		redirectAttributes.addFlashAttribute("successMessage", "Registrazione della classe effettuata con successo.");
	    return "redirect:/paginaPersonale";
	}
	
	
	
	
	@GetMapping("/deleteMateria")
	public String showDeleteMateriaForm(Model model) {
	    model.addAttribute("materie",materiaService.findAll());
		model.addAttribute("materia_scelta", new Materia());
	    return "deleteMateria";
	}
	
	
	@PostMapping("/deleteMateria")
	public String eliminaMateria(@ModelAttribute("materia_scelta") Materia materia_scelta,Model model,RedirectAttributes redirectAttributes) {
            materiaService.deleteMateriaByIdMateria(materia_scelta.getIdMateria());
        
		redirectAttributes.addFlashAttribute("successMessageDeleteMateria", "Eliminazione della materia effettuata con successo.");
	    return "redirect:/paginaPersonale";
	}
	
	
	// AZIONI CATTEDRA
	
	@GetMapping("/aggiungiCattedra")
	public String showCattedraForm(Model model) {
		model.addAttribute("materie", materiaService.findAll());
		model.addAttribute("materia_scelta", new Materia());
		model.addAttribute("classi", classeService.findAll());
		model.addAttribute("classe_scelta", new Classe());
	    return "aggiungiCattedra";
	}
	
	@RequestMapping(path="/aggiungiCattedra",method= RequestMethod.POST) 
	public String aggiungiMateria(@ModelAttribute("classe_scelta") Classe classe_scelta, RedirectAttributes redirectAttributes, @ModelAttribute("materia_scelta") Materia materia_scelta,Principal principal) {

		Classe classe = classeService.findById(classe_scelta.getIdClasse());
		Materia materia = materiaService.findById(materia_scelta.getIdMateria());
		Professore professore = (Professore)personaService.findByEmail(principal.getName());
		Cattedra cattedra = new Cattedra(professore,materia,classe);
		try{
			cattedraService.save(cattedra);
			classeService.updateCattedreClasseById(classe.getIdClasse(),cattedra);
			materiaService.updateCattedreMateriaById(materia.getIdMateria(),cattedra);
			personaService.updateCattedreProfessoreById(professore.getId(),cattedra);	
			
		} catch (CattedraAlreadyExistsException e) {
			redirectAttributes.addFlashAttribute("errorMessageCattedraAlreadyExists", "Creazione cattedra non effettuata. Cattedra giÃ  presente.");
            return "redirect:/paginaPersonale";
		}
		redirectAttributes.addFlashAttribute("successMessageCattedraAdded", "Aggiunzione cattedra effettuata con successo.");
		return "redirect:/paginaPersonale";
	}
	
	@GetMapping("/editCattedra")
	public String showEditCattedraForm(Model model) {
	    model.addAttribute("materie",materiaService.findAll());
	    model.addAttribute("classi",classeService.findAll());
	    model.addAttribute("professori",personaService.findAllProfessors());
	    model.addAttribute("materia_scelta",new Materia());
	    model.addAttribute("classe_scelta",new Classe());
	    model.addAttribute("professore_scelta",new Professore());
	    model.addAttribute("c", new Cattedra());
	    return "editCattedra";
	}
	
	
	@PostMapping("/editCattedra")
	public String modificaCattedra(@ModelAttribute("materia_scelta") Materia materia_scelta,@ModelAttribute("classe_scelta") Classe classe_scelta,
			@ModelAttribute("professore_scelta") Professore professore_scelta,Model model,RedirectAttributes redirectAttributes,Principal principal) {

		try {
			System.out.println(materia_scelta.getIdMateria());
			System.out.println(classe_scelta.getIdClasse());
			System.out.println(professore_scelta.getId());
            Cattedra cattedra1=cattedraService.findCattedra(professore_scelta, materia_scelta, classe_scelta);
            System.out.println(cattedra1.getIdCattedra());
            redirectAttributes.addFlashAttribute("cattedra_scelta", cattedra1);
            
		} catch (CattedraDoesntExistException e){
        	redirectAttributes.addFlashAttribute("errorMessageCattedra", "Modifica non effettuata, la cattedra è già presente.");
        	return "redirect:/editCattedra";
        }  catch (Exception e) {
	        	e.printStackTrace();
	        }
		return "redirect:/editCattedra2";
		
	}
	
	@GetMapping("/editCattedra2")
	public String showEditCattedraForm2(Model model, @ModelAttribute("cattedra_scelta") Cattedra cattedra) {
	    model.addAttribute("materie",materiaService.findAll());
	    model.addAttribute("classi",classeService.findAll());
	    model.addAttribute("professori",personaService.findAllProfessors());
	    model.addAttribute("materia_scelta",new Materia());
	    model.addAttribute("classe_scelta",new Classe());
	    model.addAttribute("professore_scelta",new Professore());
	    model.addAttribute("cattedra", cattedra);
	    model.addAttribute("cattedra_scelta", new Cattedra());
	    return "editCattedra2";
	}
	
	
	@PostMapping("/editCattedra2")
	public String modificaCattedra2(@ModelAttribute("materia_scelta") Materia materia_scelta,@ModelAttribute("classe_scelta") Classe classe_scelta,
			@ModelAttribute("professore_scelta") Professore professore_scelta, @ModelAttribute("cattedra_scelta") Cattedra cattedra,  Model model,RedirectAttributes redirectAttributes,Principal principal) {

		try {
			System.out.println("Second form");

			System.out.println(cattedra.getIdCattedra());
			System.out.println(materia_scelta.getIdMateria());
			System.out.println(classe_scelta.getIdClasse());
			System.out.println(professore_scelta.getId());
            Professore professore = (Professore) personaService.get(professore_scelta.getId());
            Classe classe = classeService.findById(classe_scelta.getIdClasse());
            Materia materia = materiaService.findById(materia_scelta.getIdMateria());
            cattedraService.updateCattedra(cattedra,professore, materia, classe);
		} catch (CattedraAlreadyExistsException e){
            	redirectAttributes.addFlashAttribute("errorMessageCattedra", "Modifica non effettuata, la cattedra è già presente.");
            	return "redirect:/editCattedra";
        }  catch (Exception e) {
	        	e.printStackTrace();
	        }
			
		redirectAttributes.addFlashAttribute("successMessage", "Registrazione della classe effettuata con successo.");
	    return "redirect:/paginaPersonale";
	}
	
	@GetMapping("/deleteCattedra")
	public String showDeleteCattedraForm(Model model) {
	    model.addAttribute("materie",materiaService.findAll());
	    model.addAttribute("classi",classeService.findAll());
	    model.addAttribute("professori",personaService.findAllProfessors());
	    model.addAttribute("materia_scelta",new Materia());
	    model.addAttribute("classe_scelta",new Classe());
	    model.addAttribute("professore_scelta",new Professore());
	    model.addAttribute("c", new Cattedra());
	    return "deleteCattedra";
	}
	
	
	@PostMapping("/deleteCattedra")
	public String deleteCattedra(@ModelAttribute("materia_scelta") Materia materia_scelta,@ModelAttribute("classe_scelta") Classe classe_scelta,
			@ModelAttribute("professore_scelta") Professore professore_scelta,Model model,RedirectAttributes redirectAttributes,Principal principal) {

		try {
			System.out.println(materia_scelta.getIdMateria());
			System.out.println(classe_scelta.getIdClasse());
			System.out.println(professore_scelta.getId());
            Cattedra cattedra1=cattedraService.findCattedra(professore_scelta, materia_scelta, classe_scelta);
            System.out.println(cattedra1.getIdCattedra());
            cattedraService.deleteCattedra(cattedra1.getIdCattedra());
            
		} catch (CattedraDoesntExistException e){
        	redirectAttributes.addFlashAttribute("errorMessageCattedra", "Modifica non effettuata, la cattedra è già presente.");
        	return "redirect:/deleteCattedra";
        }  catch (Exception e) {
	        	e.printStackTrace();
	        }
		redirectAttributes.addFlashAttribute("successMessageCattedra", "Modifica non effettuata, la cattedra è già presente.");
 
		return "redirect:/paginaPersonale";
		
	}
	
	
	
	//AZIONI REGISTRO CLASSE
	
	
	@GetMapping("/creaRegistroClasse")
	public String showRegisterForm(Model model, Principal principal) {
	    
		model.addAttribute("classe_scelta", new Classe());
		Professore professore = (Professore)personaService.findByEmail(principal.getName());
		List<String> namesList = professore.getCattedre().stream().map(p -> p.getClasse().getNomeClasse()).collect(Collectors.toList()); 
		model.addAttribute("nomi_classi", new HashSet<String>((namesList)));
	    return "creaRegistroClasse";
	}
	
	@PostMapping("/creaRegistroClasse")
	public String creazioneRegistroClasse(@ModelAttribute("classe_scelta") Classe classe, Model model,RedirectAttributes redirectAttributes,Principal principal) {
		try {
			
			classe = classeService.findByNome(classe.getNomeClasse());
			Professore professore = (Professore)personaService.findByEmail(principal.getName());
			RegistroClasse registro = new RegistroClasse();
			registro.setClasse(classe);
			registro.setNomeClasse(classe.getNomeClasse());
			
			registroClasseService.save(registro);
			classeService.updateRegistroClasse(classe.getIdClasse(), registro);
			
	    } catch (RegistroAlreadyExistsException e){
            redirectAttributes.addFlashAttribute("errorMessageRegisterAlreadyExists", "Creazione non effettuata. Registro di classe già presente.");
            return "redirect:/paginaPersonale";
		} catch (Exception e) {
        	e.printStackTrace();
        }
		
		redirectAttributes.addFlashAttribute("errorMessageRegisterAlreadyExists", "Creazione registro di classe effettuata con successo.");
	    return "redirect:/paginaPersonale";
	}
	
	
	@GetMapping("/mostraRegistroClasse")
	public String showRegistroClasse(Model model, Principal principal) {
		 Persona persona = personaService.findByEmail(principal.getName());
		
		 if(persona.getRuolo().equalsIgnoreCase("Professore")) {
			model.addAttribute("classe_scelta", new Classe());
			Professore professore = (Professore)persona;
			List<String> namesList = professore.getCattedre().stream().map(p -> p.getClasse().getNomeClasse()).collect(Collectors.toList()); 
			model.addAttribute("nomi_classi", new HashSet<String>((namesList)));
		}else {
			model.addAttribute("classe_scelta", new Classe());
			Studente s =(Studente)persona;
			String namesList = s.getClasse().getNomeClasse();
			model.addAttribute("nomi_classi", namesList);
		}
	    return "mostraRegistroClasse";
	}
	
	@PostMapping("/mostraRegistroClasse")
	public String showRegistroStudenti(@ModelAttribute("classe_scelta") Classe classe, Model model,Principal principal,RedirectAttributes redirectAttributes) {
		try {
			 Persona persona = personaService.findByEmail(principal.getName());
			
			 if(persona.getRuolo().equalsIgnoreCase("Studente")) {
				Studente s =(Studente)persona;
				classe= classeService.findByNome(s.getClasse().getNomeClasse());
				redirectAttributes.addFlashAttribute("studenti", classe.getStudenti());
			}else {
			classe = classeService.findByNome(classe.getNomeClasse());
			redirectAttributes.addFlashAttribute("studenti", classe.getStudenti());

			}

		}catch (Exception e) {
        	e.printStackTrace();
        	redirectAttributes.addFlashAttribute("errorMessageRegisterAlreadyExists", "Non è stata trovata nessuna lista di studenti iscritti a questa classe");
			return "redirect:/mostraRegistroClasse";
        }
		
	    return "redirect:/mostraRegistroClasse";
	}
	
	
	
	// AZIONI REGISTRO PROFESSORE
	
	
	@GetMapping("/creaRegistroProfessore")
	public String showProfessorRegisterForm(Model model) {
	    model.addAttribute("materie", materiaService.findAll());
		model.addAttribute("materia_scelta", new Materia());
		model.addAttribute("classi", classeService.findAll());
		model.addAttribute("classe_scelta", new Classe());
		
	    return "creaRegistroProfessore";
	}

	@PostMapping("/creaRegistroProfessore")
	public String creazioneRegistroProfessore(@ModelAttribute("classe_scelta") Classe classe, @ModelAttribute("materia_scelta") Materia materia_scelta, Model model,RedirectAttributes redirectAttributes,Principal principal) {
		try {

			classe = classeService.findById(classe.getIdClasse());
			Materia materia = materiaService.findById(materia_scelta.getIdMateria());
			Professore professore = (Professore)personaService.findByEmail(principal.getName());
			Cattedra cattedra = cattedraService.findCattedra(professore, materia, classe);
			
			RegistroProfessore registroProfessore = new RegistroProfessore();
			registroProfessoreService.save(registroProfessore);
			registroProfessoreService.updateCattedraRegistro(registroProfessore, cattedra);
			cattedraService.updateRegistroCattedra(registroProfessore, cattedra);
			
	    //} catch (RegistroProfessoreAlreadyExistsException e){
         //   redirectAttributes.addFlashAttribute("errorMessageRegisterAlreadyExists", "Creazione non effettuata. Registro professore già presente per questa materia.");
         //   return "redirect:/paginaPersonale";
		} catch (Exception e) {
        	e.printStackTrace();
        }
		
		redirectAttributes.addFlashAttribute("errorMessageRegisterAlreadyExists", "Creazione registro effettuata con successo.");
	    return "redirect:/paginaPersonale";
	}
	
	@GetMapping("/mostraRegistroProfessore")
	public String showRegistroProfessore(Model model, Principal principal) {
		//Professore professore = (Professore)personaService.findByEmail(principal.getName());
		model.addAttribute("materie", materiaService.findAll());
		model.addAttribute("materia_scelta", new Materia());
		model.addAttribute("classi", classeService.findAll());
		model.addAttribute("classe_scelta", new Classe());
	    return "mostraRegistroProfessore";
	}
	
	@PostMapping("/mostraRegistroProfessore")
	public String showRegistroProfessore(@ModelAttribute("classe_scelta") Classe classe,@ModelAttribute("materia_scelta") Materia materia_scelta, Model model,RedirectAttributes redirectAttributes,Principal principal) {
		try {
			 Persona persona = personaService.findByEmail(principal.getName());
				
			 if(persona.getRuolo().equalsIgnoreCase("Studente")) {
				 Studente s =(Studente)persona;
				 classe = classeService.findById(s.getClasse().getIdClasse());
				 Materia materia = materiaService.findById(materia_scelta.getIdMateria());
				 Cattedra cattedra = cattedraService.findCattedra(materia, classe);
				 RegistroProfessore registroProfessore = new RegistroProfessore();
				 registroProfessore = cattedra.getRegistroProfessore();
				 //Manca voto
				 redirectAttributes.addFlashAttribute("verifiche", s.getVerifiche()); 
				 
			 }else {
			
			classe = classeService.findById(classe.getIdClasse());
			Materia materia = materiaService.findById(materia_scelta.getIdMateria());
			Professore professore = (Professore)personaService.findByEmail(principal.getName());
			Cattedra cattedra = cattedraService.findCattedra(professore, materia, classe);
			redirectAttributes.addFlashAttribute("verifiche", cattedra.getRegistroProfessore().getVerifiche());
			 }
			 //model.addAttribute("studenti", studenti);
		
		//} catch (RegistroAlreadyExistsException e){
			
		}catch (Exception e) {
        	e.printStackTrace();
        	redirectAttributes.addFlashAttribute("errorMessageRegisterAlreadyExists", "Non è stata trovata nessuna lista di studenti iscritti a questa classe");
			return "redirect:/mostraRegistroProfessore";
        }
		
	    return "redirect:/mostraRegistroProfessore";
	}
	
	
	
	// AZIONI VERIFICA
	
	
	
	@GetMapping("/aggiungiVerifica")
	public String showVerificaForm(Model model) {
		model.addAttribute("materie", materiaService.findAll());
		model.addAttribute("materia_scelta", new Materia());
		model.addAttribute("classi", classeService.findAll());
		model.addAttribute("classe_scelta", new Classe());
		model.addAttribute("verifica", new Verifica());
		
	    return "aggiungiVerifica";
	}
	
	@RequestMapping(path="/aggiungiVerifica",method= RequestMethod.POST) 
	public String aggiungiVerifica(@ModelAttribute("classe_scelta") Classe classe_scelta, @ModelAttribute("verifica") Verifica verifica, RedirectAttributes redirectAttributes, @ModelAttribute("materia_scelta") Materia materia_scelta,Principal principal) {
		try {
				Classe classe = classeService.findById(classe_scelta.getIdClasse());
				Materia materia = materiaService.findById(materia_scelta.getIdMateria());
				Cattedra cattedra = cattedraService.findCattedra(materia, classe);
				RegistroClasse registroClasse = classe.getRegistroClasse();
				RegistroProfessore registroProfessore = new RegistroProfessore();
				registroProfessore = cattedra.getRegistroProfessore();
				
				List<Studente> myList = new ArrayList<Studente>();
				myList = classe.getStudenti();
						
				for(int i=0; i<myList.size();i++) {
					Verifica v = new Verifica();
					v.setArgomento(verifica.getArgomento());
					v.setStudente(myList.get(i));
					v.setMateria(materia);
					v.setRegistroClasse(registroClasse);
					v.setRegistroProfessore(registroProfessore);
					verificaService.save(v);
					personaService.updateVerifiche(myList.get(i), v);
					registroClasseService.updateRegistroClasseVerifiche(classe.getNomeClasse(), v);
					registroProfessoreService.updateRegistroProfessoreVerifiche(registroProfessore.getIdRegistro(), v);
					materiaService.updateMateriaVerifiche(materia.getIdMateria(), v);
						
				}
			}catch (CattedraDoesntExistException e) {
				redirectAttributes.addFlashAttribute("errorMessageCattedraDoesntExist", "La cattedra non è  presente.");
	            return "redirect:/paginaPersonale";
			}
			catch (VerificaAlreadyExistsException e) {
				redirectAttributes.addFlashAttribute("errorMessageVerificaAlreadyExists", "Creazione verifica non effettuata. Verifica giÃ  presente.");
	            return "redirect:/paginaPersonale";
			}
			redirectAttributes.addFlashAttribute("successMessageCattedraAdded", "Aggiunzione verifica effettuata con successo.");
			return "redirect:/paginaPersonale";
	}
	

	@GetMapping("/aggiungiVotoVerifica")
	public String showVotoVerificaForm(Model model,Principal principal) {
		
		model.addAttribute("verifiche", verificaService.findAll());
		model.addAttribute("verifica_scelta", new Verifica());
		model.addAttribute("classi", classeService.findAll());
		model.addAttribute("classe_scelta", new Classe());
		
		
	    return "aggiungiVotoVerifica";
	}

	@RequestMapping(path="/aggiungiVotoVerifica",method= RequestMethod.POST) 
	public String aggiungiVotoVerifica(@ModelAttribute("classe_scelta") Classe classe_scelta, @ModelAttribute("verifica_scelta") Verifica verifica_scelta, RedirectAttributes redirectAttributes, Principal principal) {

		
		Classe classe = classeService.findById(classe_scelta.getIdClasse());
		Verifica verifica = verificaService.findVerificaById(verifica_scelta.getIdVerifica());
		
		List<Verifica> verifiche = classe.getRegistroClasse().getVerifiche();
		List<Studente> studenti = classe.getStudenti();
		System.out.println(verifiche);
		
		/*for(Studente s:classe.getStudenti()) {
			 verifiche.add(new Verifica());
		 }*/
		redirectAttributes.addFlashAttribute("studenti", studenti);
		redirectAttributes.addFlashAttribute("verifica", verifica);
	
		//redirectAttributes.addFlashAttribute("list", );
		/*	try {
				
				
			} catch (VerificaAlreadyExistsException e) {
				redirectAttributes.addFlashAttribute("errorMessageVerificaAlreadyExists", "Creazione verifica non effettuata. Verifica giÃ  presente.");
	            return "redirect:/paginaPersonale";
			}*/
	
	return "redirect:/inserisciVoto";
	}
	
	@GetMapping("/inserisciVoto")
	public String showInserisciVotoVerificaForm(Model model,Principal principal, @ModelAttribute("verifica") Verifica verifica_scelta, @ModelAttribute("studenti") LinkedList<Studente> studenti) {
		List<Verifica> verifiche = new LinkedList<Verifica>();
		
		for(Studente s: studenti) {
		verifiche.add(new Verifica(s));
		}
		
		model.addAttribute("verifiche", verifiche);
		model.addAttribute("verifica_scelta", verificaService.findVerificaById(verifica_scelta.getIdVerifica()));
		return "inserisciVoto";
	}	
	
	@RequestMapping(path="/InserisciVoto",method= RequestMethod.POST) 
	public String aggiungiVoto(@ModelAttribute("verifica") List<Verifica> verifiche, @ModelAttribute("verifica_scelta") Verifica verifica, Model model, Principal principal) {
		
		verificaService.updateVoto(verifiche, verifica.getIdVerifica());
		
	
	return "redirect:/aggiungiVotoVerifica";
	}
	
	
	@GetMapping("/inserisciRappresentante")
	public String inserisciRappresentanteForm(Model model) {
		model.addAttribute("studente_scelto", new Studente());
		model.addAttribute("studenti", personaService.findAllStud());
		return "inserisciRappresentante";
	}
	
	@RequestMapping(path="/inserisciRappresentante", method=RequestMethod.POST)
	public String inserisciRappresentante(@ModelAttribute("studente_scellto") Studente studente) {
		Studente s = (Studente)personaService.findByEmail(studente.getEmail());
		personaService.updateRappresentante(s);
		personaService.updateRappresentantePersone(s);
		
		return "redirect:/paginaPersonale";
	}
	//TODO
	//Aggiungere votazione a verifica
	//Visualizzazione per lo studente del registro professore di una materia con votazioni
	
	
	
}
