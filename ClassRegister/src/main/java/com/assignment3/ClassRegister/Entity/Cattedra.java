package com.assignment3.ClassRegister.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;

@Entity
public class Cattedra {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idCattedra;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Professore professore;
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Materia materia; 
	
	@ManyToOne(cascade = CascadeType.PERSIST)
	private Classe classe;
	
	@OneToOne 
	private RegistroProfessore registroProfessore;
	
	public Cattedra() {
			
	}
	
	public Cattedra(Professore professore, Materia materia, Classe classe) {
		this.professore=professore;
		this.materia=materia;
		this.classe=classe;
	}
	
	public Long getIdCattedra() {
		return idCattedra;
	}

	public void setIdCattedra(Long idCattedra) {
		this.idCattedra = idCattedra;
	}
	
	public Professore getProfessore() {
		return professore;
	}

	public void setProfessore(Professore professore) {
		this.professore = professore;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public RegistroProfessore getRegistroProfessore() {
		return registroProfessore;
	}

	public void setRegistroProfessore(RegistroProfessore registroProfessore) {
		this.registroProfessore = registroProfessore;
	}

	@Override
	public String toString() {
		return "Cattedra [professore=" + professore.getNome() +" " + professore.getCognome()+ ", materia=" + materia.getNomeMateria() + ", classe=" + classe.getNomeClasse() + "]";
	}


	
	
	
}
