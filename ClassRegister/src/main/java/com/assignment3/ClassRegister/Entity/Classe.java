package com.assignment3.ClassRegister.Entity;
import java.util.LinkedList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "Classe", schema = "class_register")
public class Classe {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idClasse;
	
	@NotEmpty(message="Inserire il nome della classe")
	@Pattern(regexp = "[1-9][A-Z]",message="Inserire un nome valido.")
	private String nomeClasse;
	
	private Integer numeroStudenti;
	
	public Classe() {
		
	}
	
	public Classe(Long idClasse, String nomeClasse, Integer numeroStudenti) {
		this.idClasse = idClasse;
		this.nomeClasse = nomeClasse;
		this.numeroStudenti = numeroStudenti;
	}

	@OneToMany
	@Cascade(CascadeType.REMOVE)
	List<Cattedra> cattedre;
	
	@OneToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	@JoinColumn(nullable = true)
	private List<Studente> studenti = new LinkedList<Studente>();
	
	@OneToOne
	private RegistroClasse registroClasse;
	
	public Long getIdClasse() {
		return idClasse;
	}

	public void setIdClasse(Long idClasse) {
		this.idClasse = idClasse;
	}
	
	public String getNomeClasse() {
		return nomeClasse;
	}

	public void setNomeClasse(String nomeClasse) {
		this.nomeClasse = nomeClasse;
	}

	public Integer getNumeroStudenti() {
		return numeroStudenti;
	}

	public void setNumeroStudenti(Integer numeroStudenti) {
		this.numeroStudenti = numeroStudenti;
	}

	public List<Cattedra> getCattedre() {
		return cattedre;
	}

	public void setCattedre(List<Cattedra> cattedre) {
		this.cattedre = cattedre;
	}

	public List<Studente> getStudenti() {
		return studenti;
	}

	public void setStudenti(List<Studente> studenti) {
		this.studenti = studenti;
	}

	public RegistroClasse getRegistroClasse() {
		return registroClasse;
	}

	public void setRegistroClasse(RegistroClasse registroClasse) {
		this.registroClasse = registroClasse;
	}
	
	
}