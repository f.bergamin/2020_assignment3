package com.assignment3.ClassRegister.Entity;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "Materia", schema = "class_register")
public class Materia {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idMateria;
	
	@NotEmpty(message="Inserire nome della materia")
	@Pattern(regexp = "[a-zA-Z]{2,}(\\s[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)?",message="Inserire un nome valido per la materia.")
	private String nomeMateria; 
		
	public Materia() {
	}
	
	public Materia(Long idMateria) {
		this.idMateria = idMateria;
		
	}
	
	@OneToMany( orphanRemoval = true)
	@Cascade(CascadeType.REMOVE)
	private List<Cattedra> cattedre = new LinkedList<Cattedra>();
	
	@OneToMany
	private List<Verifica> verifiche;
	
	public Long getIdMateria() {
		return this.idMateria;
	}

	public void setIdMateria(Long id) {
		this.idMateria=id;
	}
	
	public String getNomeMateria() {
		return this.nomeMateria;
	}

	public void setNomeMateria(String materia) {
		this.nomeMateria=materia;
	}

	public List<Cattedra> getCattedre() {
		return cattedre;
	}

	public void setCattedre(List<Cattedra> cattedre) {
		this.cattedre = cattedre;
	}

	public List<Verifica> getVerifiche() {
		return verifiche;
	}

	public void setVerifiche(List<Verifica> verifiche) {
		this.verifiche = verifiche;
	}
	
	
}
