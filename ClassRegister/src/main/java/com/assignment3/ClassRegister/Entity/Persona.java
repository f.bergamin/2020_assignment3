package com.assignment3.ClassRegister.Entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "USER_TYPE", discriminatorType=DiscriminatorType.STRING)
public class Persona {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
    
    @NotEmpty(message="Inserire un nome.")
    @Pattern(regexp = "[a-zA-Z]{2,30}",message="Inserire un nome valido.")
	private String nome;
    
    @NotEmpty(message="Inserire un cognome.")
    @Pattern(regexp = "[a-zA-Z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?",message="Inserire un cognome valido.")
	private String cognome;
    
    @NotEmpty(message="Inserire il codice fiscale.")
    @Pattern(regexp = "[a-zA-Z]{6}\\d\\d[a-zA-Z]\\d\\d[a-zA-Z]\\d\\d\\d[a-zA-Z]",message="Codice Fiscale non valido.")
	private String codiceFiscale;
    
    @NotEmpty(message="Inserire una mail.")
    @Email(message="Inserire una mail valida.")
	private String email;
    
    @NotEmpty(message="Inserire una password.")
    @Size(min=3,message="Inserire una password con almeno 8 caratteri.")
	private String password;
    
	@Column(name="USER_TYPE",insertable=false,updatable=false)
	private String ruolo;
	
	public String getRuolo() {
		return ruolo;
	}

	public void setRuolo(String ruolo) { 
		this.ruolo = ruolo;
	}

	public Persona() {
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getCodiceFiscale() {
		return codiceFiscale;
	}

	public void setCodiceFiscale(String codiceFiscale) {
		this.codiceFiscale = codiceFiscale;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	

}
