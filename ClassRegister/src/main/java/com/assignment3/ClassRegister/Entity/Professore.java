package com.assignment3.ClassRegister.Entity;

import java.util.LinkedList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.OneToMany;


@Entity
@DiscriminatorValue("Professore")
public class Professore extends Persona {
	
	public Professore() {
		super();
	}

	@OneToMany
	List<Cattedra> cattedre = new LinkedList<Cattedra>(); 
	
	@Override
	public String toString() {
		return super.toString();
	}

	public List<Cattedra> getCattedre() {
		return this.cattedre;
	}

	public void setCattedre(List<Cattedra> cattedre) {
		this.cattedre = cattedre;
	}



	
	
	
}