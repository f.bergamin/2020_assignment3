package com.assignment3.ClassRegister.Entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class RegistroClasse {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idRegistroClasse;
	private String nomeClasse;
	
	public RegistroClasse() {
		
	}
	
	public RegistroClasse(Long idRegistroClasse, String nomeClasse) {
		this.idRegistroClasse = idRegistroClasse;
		this.nomeClasse = nomeClasse;
	}
	
	@OneToMany
	private List<Verifica> verifiche;
	
	@OneToOne
	private Classe classe;
	
	@OneToMany
	private List<Materia> materie;
	
	public String getNomeClasse() {
		return nomeClasse;
	}

	public void setNomeClasse(String nomeClasse) {
		this.nomeClasse = nomeClasse;
	}
	
	public Long getIdRegistroClasse() {
		return idRegistroClasse;
	}

	public void setIdRegistroClasse(Long idRegistroClasse) {
		this.idRegistroClasse = idRegistroClasse;
	}

	public List<Verifica> getVerifiche() {
		return verifiche;
	}

	public void setVerifiche(List<Verifica> verifiche) {
		this.verifiche = verifiche;
	}

	public List<Materia> getMaterie() {
		return materie;
	}

	public void setMaterie(List<Materia> materie) {
		this.materie = materie;
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	
}
