package com.assignment3.ClassRegister.Entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
public class RegistroProfessore {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idRegistro;
	
	public RegistroProfessore() {
		
	}
	
	public RegistroProfessore(Long idRegistro) {
		this.idRegistro = idRegistro;
	}
	
	@ManyToOne
	private Studente studente;
	
	@OneToMany
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Verifica> verifiche;
	
	@OneToOne
	private Cattedra cattedra;
	

	public Long getIdRegistro() {
		return idRegistro;
	}

	public void setIdRegistro(Long idRegistro) {
		this.idRegistro = idRegistro;
	}

	public List<Verifica> getVerifiche() {
		return verifiche;
	}

	public void setVerifiche(List<Verifica> verifiche) {
		this.verifiche = verifiche;
	}

	public Cattedra getCattedra() {
		return cattedra;
	}

	public void setCattedra(Cattedra cattedra) {
		this.cattedra = cattedra;
	}

	public Studente getStudente() {
		return studente;
	}

	public void setStudente(Studente studente) {
		this.studente = studente;
	}

	

}
