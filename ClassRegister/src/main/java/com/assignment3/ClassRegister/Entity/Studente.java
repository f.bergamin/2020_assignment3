package com.assignment3.ClassRegister.Entity;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@DiscriminatorValue("Studente")
public class Studente extends Persona{
	@Column(name="idRappresentante",insertable=false,updatable=false)
	private Long idRappresentante;
	public Studente() {
		super();
		// TODO Auto-generated constructor stub
	}

	@ManyToOne(cascade=CascadeType.ALL)
	private Classe classe;
	
	@OneToMany
	private List<RegistroProfessore> registriProfessori;
	
	
	@ManyToMany(cascade={CascadeType.ALL})
	private List<Materia> materie;
	
	@OneToMany(fetch = FetchType.EAGER)
	private List<Verifica> verifiche = new LinkedList<Verifica>();
	
	@ManyToOne(cascade={CascadeType.ALL})
	@JoinColumn(name="idRappresentante")
	private Studente rappresentante;
	
	@OneToMany(mappedBy="rappresentante")
	private List<Studente> studenti_r = new LinkedList<Studente>();
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	public Classe getClasse() {
		return classe;
	}

	public void setClasse(Classe classe) {
		this.classe = classe;
	}
	
	public List<Materia> getMaterie() {
		return materie;
	}

	public void setMaterie(List<Materia> materie) {
		this.materie = materie;
	}

	public List<RegistroProfessore> getRegistriProfessori() {
		return registriProfessori;
	}

	public void setRegistriProfessori(List<RegistroProfessore> registriProfessori) {
		this.registriProfessori = registriProfessori;
	}

	public List<Verifica> getVerifiche() {
		return verifiche;
	}

	public void setVerifiche(List<Verifica> verifiche) {
		this.verifiche = verifiche;
	}

	public Long getIdRappresentante() {
		return idRappresentante;
	}

	public void setIdRappresentante(Long idRappresentante) {
		this.idRappresentante = idRappresentante;
	}

	public Studente getRappresentante() {
		return rappresentante;
	}

	public void setRappresentante(Studente rappresentante) {
		this.rappresentante = rappresentante;
	}

	public List<Studente> getStudenti_r() {
		return studenti_r;
	}

	public void setStudenti_r(List<Studente> studenti_r) {
		this.studenti_r = studenti_r;
	}

	

	
	
	
}
