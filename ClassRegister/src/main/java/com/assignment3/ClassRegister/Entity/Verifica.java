package com.assignment3.ClassRegister.Entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Verifica {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long idVerifica;
	private int voto;
	private String argomento;
	
	public Verifica() {
		
	}
	
	public Verifica(Studente s) {
		this.studente=s;
	}

	@ManyToOne
	private RegistroClasse registroClasse;
	
	@ManyToOne
	private Materia materia;
	
	@ManyToOne
	private RegistroProfessore registroProfessore;

	@ManyToOne(cascade={CascadeType.ALL})
	private Studente studente;
	
	public Long getIdVerifica() {
		return idVerifica;
	}

	public void setIdVerifica(Long idVerifica) {
		this.idVerifica = idVerifica;
	}

	public String getArgomento() {
		return argomento;
	}

	public void setArgomento(String argomento) {
		this.argomento = argomento;
	}

	public int getVoto() {
		return voto;
	}

	public void setVoto(int voto) {
		this.voto = voto;
	}

	public RegistroClasse getRegistroClasse() {
		return registroClasse;
	}

	public void setRegistroClasse(RegistroClasse registroClasse) {
		this.registroClasse = registroClasse;
	}

	public Materia getMateria() {
		return materia;
	}

	public void setMateria(Materia materia) {
		this.materia = materia;
	}

	public RegistroProfessore getRegistroProfessore() {
		return registroProfessore;
	}

	public void setRegistroProfessore(RegistroProfessore registroProfessore) {
		this.registroProfessore = registroProfessore;
	}

	public Studente getStudente() {
		return studente;
	}

	public void setStudente(Studente studente) {
		this.studente = studente;
	}

	

	
	
	
}
