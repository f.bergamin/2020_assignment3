package com.assignment3.ClassRegister.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.assignment3.ClassRegister.Entity.Cattedra;
import com.assignment3.ClassRegister.Entity.Materia;

@Repository
public interface CattedraRepository extends JpaRepository<Cattedra, Long>  {
	
	public Cattedra findCattedraByIdCattedra(Long idMateria);
	
	@Query("SELECT c FROM Cattedra c WHERE LOWER(c.professore.id) = LOWER(:id) AND LOWER(c.materia.idMateria) = LOWER(:idMateria) AND LOWER(c.classe.idClasse) = LOWER(:idClasse)")
	public Cattedra findByProfessoreMateriaClasse(Long id, Long idMateria, Long idClasse);

	@Query("SELECT c FROM Cattedra c WHERE LOWER(c.materia.id) = LOWER(:idMateria) AND LOWER(c.classe.idClasse) = LOWER(:idClasse)")
	public Cattedra findByMateriaClasse(Long idMateria, Long idClasse);


}
