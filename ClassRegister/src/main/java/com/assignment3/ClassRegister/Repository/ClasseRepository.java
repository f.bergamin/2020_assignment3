package com.assignment3.ClassRegister.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.assignment3.ClassRegister.Entity.Cattedra;
import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Entity.Persona;
import com.assignment3.ClassRegister.Error.UserAlreadyExistsException;

@Repository
public interface ClasseRepository extends JpaRepository<Classe, Long>  {
	
	public Classe findClasseByIdClasse(Long idClasse);

	public Classe findByNomeClasse(String nomeClasse);


}