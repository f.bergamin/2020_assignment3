package com.assignment3.ClassRegister.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


import com.assignment3.ClassRegister.Entity.Materia;

@Repository
public interface MateriaRepository extends JpaRepository<Materia, Long>  {
	
	public Materia findMateriaByIdMateria(Long idMateria);

	public Materia findByNomeMateria(String nomeMateria);

	public void deleteMateriaByIdMateria(Long idMateria);

}
