package com.assignment3.ClassRegister.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.assignment3.ClassRegister.Entity.Persona;
import com.assignment3.ClassRegister.Entity.Professore;
import com.assignment3.ClassRegister.Entity.Studente;

@Repository
public interface PersonaRepository extends JpaRepository<Persona, Long>  {
	
	public Persona findPersonaById(Long id);
	public Persona findByEmail(String email);
	public Persona findByEmailAndPassword(String email,String password);

	@Query("SELECT c FROM Persona c WHERE c.ruolo = 'Professore'")
	public List<Professore> findAllProfessors();
	
	@Query("SELECT c FROM Persona c WHERE c.ruolo = 'Studente'")
	public List<Studente> findAllStudents();

}
