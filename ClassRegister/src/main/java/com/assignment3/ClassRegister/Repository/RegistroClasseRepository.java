package com.assignment3.ClassRegister.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Entity.Persona;
import com.assignment3.ClassRegister.Entity.RegistroClasse;
import com.assignment3.ClassRegister.Error.UserAlreadyExistsException;

@Repository
public interface RegistroClasseRepository extends JpaRepository<RegistroClasse, Long>  {
	
	//public RegistroClasse findRegistroById(Long idRegistroClasse);
	public RegistroClasse findRegistroByNomeClasse(String nomeClasse);



	

}
