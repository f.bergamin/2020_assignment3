package com.assignment3.ClassRegister.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.assignment3.ClassRegister.Entity.RegistroProfessore;


@Repository
public interface RegistroProfessoreRepository extends JpaRepository<RegistroProfessore, Long>  {
	
	//public RegistroClasse findRegistroById(Long idRegistroClasse);
	public RegistroProfessore findRegistroBycattedra(Long idCattedra);
	public RegistroProfessore findRegistroByIdRegistro(Long idRegistro);
	


	

}
