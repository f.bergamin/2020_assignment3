package com.assignment3.ClassRegister.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.assignment3.ClassRegister.Entity.Studente;
import com.assignment3.ClassRegister.Entity.Verifica;

@Repository
public interface VerificaRepository extends JpaRepository<Verifica, Long>   {
	public Verifica findVerificaByIdVerifica(Long idVerifica);

	public List<Verifica> findAllVerificaByIdVerifica(Long idVerifica);
	//public List<Verifica> findAllVerificaByArgomento(List<Studente> studenti, String argomento);
}
