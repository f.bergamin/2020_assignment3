package com.assignment3.ClassRegister.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment3.ClassRegister.Error.CattedraDoesntExistException;
import com.assignment3.ClassRegister.Entity.Cattedra;
import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Entity.Materia;
import com.assignment3.ClassRegister.Entity.Professore;
import com.assignment3.ClassRegister.Entity.RegistroProfessore;
import com.assignment3.ClassRegister.Error.CattedraAlreadyExistsException;
import com.assignment3.ClassRegister.Repository.CattedraRepository;
import com.assignment3.ClassRegister.Repository.ClasseRepository;
import com.assignment3.ClassRegister.Repository.MateriaRepository;
import com.assignment3.ClassRegister.Repository.PersonaRepository;


@Service
public class CattedraService {
	@Autowired
	private CattedraRepository repo;
	@Autowired
	private MateriaRepository materiaRepo;
	@Autowired
	private ClasseRepository classeRepo;
	@Autowired
	private PersonaRepository personaRepo;
	
	//C
	public void save(Cattedra cattedra) throws CattedraAlreadyExistsException {
		
		if (repo.findByProfessoreMateriaClasse(cattedra.getProfessore().getId(),cattedra.getMateria().getIdMateria(),cattedra.getClasse().getIdClasse())==null) {
			repo.save(cattedra);
		} else {
			throw new CattedraAlreadyExistsException();
		}
		
	}
	//R
	public List<Cattedra> findAll() {
		return repo.findAll();
	}

	public Cattedra findById(Long idCattedra) {
		return repo.findCattedraByIdCattedra(idCattedra);
	}
	
	public Cattedra findCattedra(Professore professore, Materia materia,Classe classe) throws CattedraDoesntExistException {
		Cattedra cattedra = repo.findByProfessoreMateriaClasse(professore.getId(),materia.getIdMateria(), classe.getIdClasse());
		if (cattedra!=null) {
			return cattedra;
		}
		else {
			throw new CattedraDoesntExistException();
			}
	}
	
	public Cattedra findCattedra(Materia materia,Classe classe) throws CattedraDoesntExistException {
		Cattedra cattedra = repo.findByMateriaClasse(materia.getIdMateria(), classe.getIdClasse());
		if (cattedra!=null) {
			return cattedra;
		}
		else {
			throw new CattedraDoesntExistException();
			}
	}
	
	//U
	public void updateRegistroCattedra(RegistroProfessore registroProfessore, Cattedra cattedra) {
		cattedra.setRegistroProfessore(registroProfessore);
		repo.save(cattedra);
	}
	
	public void deleteCattedra(Long id) {
		Cattedra cattedra = repo.findCattedraByIdCattedra(id);
		Materia materia = materiaRepo.findMateriaByIdMateria(cattedra.getMateria().getIdMateria());
		List<Cattedra> lista = materia.getCattedre();
		System.out.println(lista.remove(cattedra));lista.remove(cattedra);
		materia.setCattedre(lista);
		materiaRepo.save(materia);
		cattedra.setMateria(null);
		Classe classe = classeRepo.findClasseByIdClasse(cattedra.getClasse().getIdClasse());
		lista = classe.getCattedre();
		System.out.println(lista.remove(cattedra));lista.remove(cattedra);
		classe.setCattedre(lista);
		classeRepo.save(classe);
		cattedra.setClasse(null);
		Professore professore = (Professore) personaRepo.findPersonaById(cattedra.getProfessore().getId());
		lista = professore.getCattedre();
		System.out.println(lista.remove(cattedra));lista.remove(cattedra);
		professore.setCattedre(lista);
		personaRepo.save(professore);
		cattedra.setProfessore(null);
		
		repo.deleteById(cattedra.getIdCattedra());
	}
	
	public void updateCattedra(Cattedra catt,Professore professore_scelta2, Materia materia_scelta2, Classe classe_scelta2) throws CattedraAlreadyExistsException  {
		Cattedra cattedra = repo.findCattedraByIdCattedra(catt.getIdCattedra());
		if(repo.findByProfessoreMateriaClasse(professore_scelta2.getId(), materia_scelta2.getIdMateria(), classe_scelta2.getIdClasse())==null) {
			cattedra.setClasse(classe_scelta2);
			cattedra.setMateria(materia_scelta2);
			cattedra.setProfessore(professore_scelta2);
			repo.save(cattedra);
			
		} else {
			throw new CattedraAlreadyExistsException();
		}
	}
}
