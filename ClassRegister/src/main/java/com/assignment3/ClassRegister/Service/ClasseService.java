package com.assignment3.ClassRegister.Service;

import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.assignment3.ClassRegister.Entity.Cattedra;
import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Entity.Professore;
import com.assignment3.ClassRegister.Entity.RegistroClasse;
import com.assignment3.ClassRegister.Entity.RegistroProfessore;
import com.assignment3.ClassRegister.Entity.Studente;
import com.assignment3.ClassRegister.Error.ClasseAlreadyExistsException;
import com.assignment3.ClassRegister.Repository.ClasseRepository;

@Service
public class ClasseService {
	@Autowired
	private ClasseRepository repo;
	@Autowired
	private CattedraService cattedraService;
	
	//C
	public void save(Classe classe) throws ClasseAlreadyExistsException {
		System.out.println(classe.getNomeClasse());
		
		if (repo.findByNomeClasse(classe.getNomeClasse())==null) {
			repo.save(classe);
		} else {
			throw new ClasseAlreadyExistsException();
		}
		
	}
	//R
	public List<Classe> findAll() {
		return repo.findAll();
	}
	

	public Classe findById(Long idClasse) {
		return repo.findClasseByIdClasse(idClasse);
	}
	
	public Classe findByNome(String nomeClasse) {
		return repo.findByNomeClasse(nomeClasse);
	}
	
	public List<Studente> findStudByClasse(Long idClasse){
		Classe classe = repo.findClasseByIdClasse(idClasse);
		return classe.getStudenti();
	}
	
	//U
	public void updateStudentiClasse(Studente studente, Long idClasse) {
		Classe classe = repo.findClasseByIdClasse(idClasse);
		List<Studente> studenti = classe.getStudenti();
		studenti.add(studente);
		classe.setStudenti(studenti);
		repo.save(classe);
		
	}
	
	public void updateRegistroClasse(Long idClasse, RegistroClasse registro) {
		Classe classe = repo.findClasseByIdClasse(idClasse);
		classe.setRegistroClasse(registro);
		repo.save(classe);
	}
	
	public void updateCattedreClasseById(Long idClasse, Cattedra cattedra) {
		Classe classe =  repo.findClasseByIdClasse(idClasse);
		List<Cattedra> lista = classe.getCattedre();
		lista.add(cattedra);
		classe.setCattedre(lista);
		repo.save(classe);
	}
	
	public void updateClasse(Long idClasse, @Valid Classe classe) throws ClasseAlreadyExistsException {
		Classe c = repo.findClasseByIdClasse(idClasse);
		if(classe.getNomeClasse()=="") {
			classe.setNomeClasse(c.getNomeClasse());
		}
		if(classe.getNumeroStudenti()==null) {
			classe.setNumeroStudenti(c.getNumeroStudenti());
		}
		Classe classe_trovata = repo.findByNomeClasse(classe.getNomeClasse());
		if(classe_trovata==null) {
			c.setNomeClasse(classe.getNomeClasse());
			c.setNumeroStudenti(classe.getNumeroStudenti());
		} else if(classe_trovata.getIdClasse()==c.getIdClasse()) {
			c.setNumeroStudenti(classe.getNumeroStudenti());
		} else {
			throw new ClasseAlreadyExistsException();
		}
		repo.save(c);
		
	}
	
	
	public void deleteClasseById(Long idClasse) {
		Classe classe = repo.findClasseByIdClasse(idClasse);
		List<Cattedra> cattedre=classe.getCattedre();
		Iterator<Cattedra> iter = cattedre.iterator();
		while (iter.hasNext()) {
		    Cattedra cattedra = iter.next();
			cattedraService.deleteCattedra(cattedra.getIdCattedra());
		}
		repo.deleteById(idClasse);
		
	}
}
