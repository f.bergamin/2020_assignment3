package com.assignment3.ClassRegister.Service;

import java.util.Iterator;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment3.ClassRegister.Entity.Cattedra;
import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Entity.Materia;
import com.assignment3.ClassRegister.Entity.Professore;
import com.assignment3.ClassRegister.Entity.RegistroProfessore;
import com.assignment3.ClassRegister.Entity.Verifica;
import com.assignment3.ClassRegister.Error.MateriaAlreadyExistsException;
import com.assignment3.ClassRegister.Repository.ClasseRepository;
import com.assignment3.ClassRegister.Repository.MateriaRepository;

@Service
public class MateriaService {
	@Autowired
	private MateriaRepository repo;
	@Autowired
	private CattedraService cattedraService;
	
	//C
	public void save(Materia materia) throws MateriaAlreadyExistsException {
		System.out.println(materia.getNomeMateria());
		
		if (repo.findByNomeMateria(materia.getNomeMateria())==null) {
			repo.save(materia);
		} else {
			throw new MateriaAlreadyExistsException();
		}
		
	}
	
	//R
	public List<Materia> findAll() {
		return repo.findAll();
	}

	public Materia findById(Long idMateria) {
		return repo.findMateriaByIdMateria(idMateria);
	}
	
	//U
	public void updateCattedreMateriaById(Long idMateria, Cattedra cattedra)  {
		Materia materia = repo.findMateriaByIdMateria(idMateria);
		List<Cattedra> lista = materia.getCattedre();
		lista.add(cattedra);
		materia.setCattedre(lista);
		repo.save(materia);
	}
	
	public void updateMateriaVerifiche(Long idMateria, Verifica verifica) {
		Materia materia = repo.findMateriaByIdMateria(idMateria);
		List<Verifica> verifiche = materia.getVerifiche();
		verifiche.add(verifica);
		materia.setVerifiche(verifiche);
		repo.save(materia);
	}

	public void updateMateria(Long idMateria, @Valid Materia materia_scelta) throws MateriaAlreadyExistsException {
		Materia materia = repo.findMateriaByIdMateria(idMateria);
		if(repo.findByNomeMateria(materia_scelta.getNomeMateria())==null) {
			materia.setNomeMateria(materia_scelta.getNomeMateria());
			repo.save(materia);
		} else {
			throw new MateriaAlreadyExistsException();
		}
		
	}

	public void deleteMateriaByIdMateria(Long idMateria) {
		Materia materia = repo.findMateriaByIdMateria(idMateria);
		List<Cattedra> cattedre=materia.getCattedre();
		Iterator<Cattedra> iter = cattedre.iterator();
		while (iter.hasNext()) {
		    Cattedra cattedra = iter.next();
			cattedraService.deleteCattedra(cattedra.getIdCattedra());
		}
		repo.deleteById(idMateria);
		
	}
}
