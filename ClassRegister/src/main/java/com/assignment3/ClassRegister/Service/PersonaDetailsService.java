package com.assignment3.ClassRegister.Service;

import java.util.Collections;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.assignment3.ClassRegister.Entity.Persona;
import com.assignment3.ClassRegister.Repository.PersonaRepository;



@Service
@Transactional
public class PersonaDetailsService implements UserDetailsService {

  @Autowired
  private PersonaRepository personaRepository;


  @Override
  public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
      Persona user = personaRepository.findByEmail(userName);
     if (user == null) throw new UsernameNotFoundException("Email " + userName + " not found");
       return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), Collections.singleton(new SimpleGrantedAuthority(user.getRuolo())));
  }
}