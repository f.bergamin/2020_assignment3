package com.assignment3.ClassRegister.Service;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.assignment3.ClassRegister.Entity.Cattedra;
import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Entity.Persona;
import com.assignment3.ClassRegister.Entity.Professore;
import com.assignment3.ClassRegister.Entity.Studente;
import com.assignment3.ClassRegister.Entity.Verifica;
import com.assignment3.ClassRegister.Error.StudentAlreadyEnrolledException;
import com.assignment3.ClassRegister.Error.UserAlreadyExistsException;
import com.assignment3.ClassRegister.Repository.CattedraRepository;
import com.assignment3.ClassRegister.Repository.PersonaRepository;

@Service
public class PersonaService {
	@Autowired
	private PersonaRepository repo;
	@Autowired
	private PasswordEncoder passwordEncoder;
	@Autowired
	private CattedraService cattedraService;
	//C
	public void save(Persona persona) throws UserAlreadyExistsException {
			System.out.println(persona.getEmail());
			persona.setPassword(passwordEncoder.encode(persona.getPassword()));
			if (repo.findByEmail(persona.getEmail())==null) {
				repo.save(persona);
			} else {
				throw new UserAlreadyExistsException();
			}
		}

	
	//R
	public Persona login(String email, String password) {
		Persona persona = repo.findByEmailAndPassword(email,password);
		return persona;
	}
	
	public Persona get(Long id) {
		Persona persona = repo.findPersonaById(id);
		return persona;
	}

	public Persona findByEmail(String email) {
		Persona persona = repo.findByEmail(email);
		return persona;
	}
	
	public List<Persona> findAll(){
		return repo.findAll();
	}
	
	public List<Studente> findAllStud(){
		return repo.findAllStudents();
	}
	
	//U
	public void updateClasseStudenteById(Long id,Classe classe) throws StudentAlreadyEnrolledException {
		Studente stud = (Studente) repo.findPersonaById(id);
		if (stud.getClasse()==null) {
			System.out.println(stud.getClasse());
			stud.setClasse(classe);
			repo.save(stud);
		} else throw new StudentAlreadyEnrolledException();
		
	}
	public void updateCattedreProfessoreById(Long id, Cattedra cattedra) {
		Professore professore =  (Professore) repo.findPersonaById(id);
		List<Cattedra> lista = professore.getCattedre();
		lista.add(cattedra);
		professore.setCattedre(lista);
		repo.save(professore);
	}
	
	public void updateVerifiche(Studente s, Verifica verifica) {
		List<Verifica> verifiche = s.getVerifiche();
		verifiche.add(verifica);
		s.setVerifiche(verifiche);
		repo.save(s);
	}
	
	public void updateRappresentante(Studente s) {
		List<Studente> persone = repo.findAllStudents();
		
		for(int i=0; i< persone.size();i++) {
			if(persone.get(i).getRuolo().equalsIgnoreCase("Studente")) {
			Studente p = new Studente();
			p = persone.get(i);
			if(p.getId() == s.getId()) {
				p.setRappresentante(null);
				repo.save(p);
			}else {
				p.setRappresentante(s);
				repo.save(p);
			}
			}
		}
		
	}
	
	public void updateRappresentantePersone(Studente s) {
		List<Studente> studenti = repo.findAllStudents();
		studenti.remove(s);
		s.setStudenti_r(studenti);
		repo.save(s);
		}
	
	//D
	public void deleteById(Long id) {
		Persona persona = repo.findPersonaById(id);
		if(persona.getRuolo().equalsIgnoreCase("Professore")) {
			Professore professore = (Professore) persona;
			List<Cattedra> cattedre=professore.getCattedre();
			Iterator<Cattedra> iter = cattedre.iterator();
			while (iter.hasNext()) {
			    Cattedra cattedra = iter.next();
			    System.out.println(cattedra.toString());
				cattedraService.deleteCattedra(cattedra.getIdCattedra());
			}
		}
		repo.deleteById(id);
	}


	public void updatePersonaById(Long id, @Valid Persona persona) {
		BCryptPasswordEncoder passencoder = new BCryptPasswordEncoder();
		 Persona p = repo.findPersonaById(id);
		 p.setCodiceFiscale(persona.getCodiceFiscale());
		 p.setNome(persona.getNome());
		 p.setCognome(persona.getCognome());
		 p.setEmail(persona.getEmail());
		 p.setPassword(passencoder.encode(persona.getPassword()));
		 repo.save(p);
		
	}


	public List<Professore> findAllProfessors() {
		return repo.findAllProfessors();
	}





	


	

}
