package com.assignment3.ClassRegister.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Entity.RegistroClasse;
import com.assignment3.ClassRegister.Entity.Studente;
import com.assignment3.ClassRegister.Entity.Verifica;
import com.assignment3.ClassRegister.Error.RegistroAlreadyExistsException;
import com.assignment3.ClassRegister.Repository.RegistroClasseRepository;

@Service
public class RegistroClasseService {
	@Autowired
	private  RegistroClasseRepository repo;
	
	//C
	public void save(RegistroClasse registroClasse) throws RegistroAlreadyExistsException {
		
		if (repo.findRegistroByNomeClasse(registroClasse.getNomeClasse())==null){
			
			repo.save(registroClasse);
		} else {
			throw new RegistroAlreadyExistsException();
		}
		
	}
	//R
	public List<Studente> findAllStudentiClasse(Classe classe) throws RegistroAlreadyExistsException {
		List<Studente> studenti = classe.getStudenti();
		if(studenti.isEmpty())
			throw new RegistroAlreadyExistsException();
		return studenti;
	}
	
	//U
	public void updateRegistroClasseVerifiche(String nomeClasse, Verifica verifica) {
		RegistroClasse classe = repo.findRegistroByNomeClasse(nomeClasse);
		List<Verifica> verifiche = classe.getVerifiche();
		verifiche.add(verifica);
		classe.setVerifiche(verifiche);
		repo.save(classe);
	}	
			
		
}
