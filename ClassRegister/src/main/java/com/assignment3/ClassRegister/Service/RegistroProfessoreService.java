package com.assignment3.ClassRegister.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment3.ClassRegister.Entity.Cattedra;
import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Entity.RegistroClasse;
import com.assignment3.ClassRegister.Entity.RegistroProfessore;
import com.assignment3.ClassRegister.Entity.Studente;
import com.assignment3.ClassRegister.Entity.Verifica;
import com.assignment3.ClassRegister.Error.RegistroAlreadyExistsException;
import com.assignment3.ClassRegister.Repository.ClasseRepository;
import com.assignment3.ClassRegister.Repository.RegistroClasseRepository;
import com.assignment3.ClassRegister.Repository.RegistroProfessoreRepository;

@Service
public class RegistroProfessoreService {
	@Autowired
	private  RegistroProfessoreRepository repo;
	
	//C
	public void save(RegistroProfessore registroProfessore) throws RegistroProfessoreAlreadyExistsException {
		
		//if(repo.findRegistroBycattedra(registroProfessore.getCattedra().getIdCattedra())!=null){
			
			repo.save(registroProfessore);
		//} else {
		//	throw new RegistroProfessoreAlreadyExistsException();
		//}
	}
	//R
	public List<Studente> findAllStudentiClasse(Classe classe) throws RegistroAlreadyExistsException {
		List<Studente> studenti = classe.getStudenti();
		if(studenti.isEmpty())
			throw new RegistroAlreadyExistsException();
			return studenti;
	}
	
	public RegistroProfessore findRegistro(Long idCattedra) {
		return repo.findRegistroBycattedra(idCattedra);
	}
			
	
	//U
	public void updateCattedraRegistro(RegistroProfessore registroProfessore, Cattedra cattedra) {
		registroProfessore.setCattedra(cattedra);
		repo.save(registroProfessore);
	}
	
	public void updateRegistroProfessoreVerifiche(Long idRegistro, Verifica verifica) {
		RegistroProfessore registroProfessore=repo.findRegistroByIdRegistro(idRegistro);
		List<Verifica> verifiche = registroProfessore.getVerifiche();
		verifiche.add(verifica);
		registroProfessore.setVerifiche(verifiche);
		repo.save(registroProfessore);
	}
	
	
		
			
		
}
