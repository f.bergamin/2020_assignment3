package com.assignment3.ClassRegister.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.assignment3.ClassRegister.Entity.Materia;
import com.assignment3.ClassRegister.Entity.RegistroClasse;
import com.assignment3.ClassRegister.Entity.RegistroProfessore;
import com.assignment3.ClassRegister.Entity.Studente;
import com.assignment3.ClassRegister.Entity.Verifica;
import com.assignment3.ClassRegister.Error.VerificaAlreadyExistsException;
import com.assignment3.ClassRegister.Repository.VerificaRepository;

@Service
public class VerificaService {
	@Autowired
	private VerificaRepository repo;
	
	//C
	public void save(Verifica verifica) throws VerificaAlreadyExistsException {
		System.out.println(verifica.getIdVerifica());
		if (repo.findVerificaByIdVerifica(verifica.getIdVerifica())==null) {
			repo.save(verifica);
		} else {
			throw new VerificaAlreadyExistsException();
		}
		
	}
	//R
	public List<Verifica> findAll() {
		return repo.findAll();
	}
	
	public Verifica findVerificaById(Long idVerifica) {
		return repo.findVerificaByIdVerifica(idVerifica);
	}
	
	//public List<Verifica> findVerificaByArgomento(List<Studente> s,String argomento){
		//return repo.findAllVerificaByArgomento(s, argomento);
	//}
	
	//U
	public void updateRegistroClasse(Long idVerifica, RegistroClasse registro) {
		Verifica verifica = repo.findVerificaByIdVerifica(idVerifica);
		verifica.setRegistroClasse(registro);
		repo.save(verifica);
	}
	
	public void updateRegistroProfessore(Long idVerifica, RegistroProfessore registro) {
		Verifica verifica = repo.findVerificaByIdVerifica(idVerifica);
		verifica.setRegistroProfessore(registro);
		repo.save(verifica);
	}
	
	public void updateMateria(Long idVerifica, Materia materia) {
		Verifica verifica = repo.findVerificaByIdVerifica(idVerifica);
		verifica.setMateria(materia);
		repo.save(verifica);
	}
	
	public void updateArgomento(Long idVerifica, String argomento) {
		Verifica verifica = repo.findVerificaByIdVerifica(idVerifica);
		verifica.setArgomento(argomento.toString());
		repo.save(verifica);
	}
	public void updateVoto(List<Verifica> verifiche, Long idVerifica) {
		// TODO Auto-generated method stub
		
	}
	
	

}
