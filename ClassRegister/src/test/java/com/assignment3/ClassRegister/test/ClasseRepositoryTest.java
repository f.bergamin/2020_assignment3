package com.assignment3.ClassRegister.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.junit.jupiter.api.Order;
import org.springframework.test.annotation.Rollback;
import org.junit.jupiter.api.MethodOrderer;

import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Repository.ClasseRepository;



@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ClasseRepositoryTest {

    @Autowired
    private ClasseRepository repository;
     
    @Test
    @Rollback(false)
    @Order(1)
    public void testSaveNewClasse() {
    	Classe classe = new Classe();
    	classe.setNomeClasse("1X");
    	classe.setNumeroStudenti(1);
        Classe classe_salvata = repository.save(classe);
                 
        assertThat(classe_salvata.getIdClasse()).isGreaterThan(0);
    }
    
    
    @Test
    @Order(2)
    public void testFindClasse() {
    	Classe classe_trovata = repository.findByNomeClasse("1X");
                 
        assertThat(classe_trovata.getNomeClasse()=="1X");
    }
    
    @Test
    @Rollback(false)
    @Order(3)
    public void testUpdateClasse() {
    	Classe classe = repository.findByNomeClasse("1X");
        classe.setNomeClasse("1Y");
        classe.setNumeroStudenti(2);
        repository.save(classe);
        
    	Classe classe_modificata = repository.findByNomeClasse("1Y");
        assertThat(classe_modificata.getNumeroStudenti()==2);
    }
    
    @Test
    @Order(4)
    public void testListClassi() {
    	
        List<Classe> classi = repository.findAll();
        assertThat(classi).size().isGreaterThan(0);
    }
    
    @Test
    @Rollback(false)
    @Order(5)
    public void testDeleteClasse() {
    	Classe classe = repository.findByNomeClasse("1Y");
        repository.deleteById(classe.getIdClasse());
         
        Classe deletedClasse = repository.findByNomeClasse("1Y");
         
        assertThat(deletedClasse).isNull();       
         
    }
}
