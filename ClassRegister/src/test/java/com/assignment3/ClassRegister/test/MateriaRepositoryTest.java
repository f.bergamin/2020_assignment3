package com.assignment3.ClassRegister.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.junit.jupiter.api.Order;
import org.springframework.test.annotation.Rollback;
import org.junit.jupiter.api.MethodOrderer;

import com.assignment3.ClassRegister.Entity.Classe;
import com.assignment3.ClassRegister.Entity.Materia;
import com.assignment3.ClassRegister.Repository.ClasseRepository;
import com.assignment3.ClassRegister.Repository.MateriaRepository;



@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class MateriaRepositoryTest {

    @Autowired
    private MateriaRepository repository;
     
    @Test
    @Rollback(false)
    @Order(1)
    public void testSaveNewMateria() {
    	Materia materia = new Materia();
    	materia.setNomeMateria("ItalianoTest");
        Materia materia_salvata = repository.save(materia);
                 
        assertThat(materia_salvata.getIdMateria()).isGreaterThan(0);
    }
    
    
    @Test
    @Order(2)
    public void testFindMateria() {
    	Materia materia_trovata = repository.findByNomeMateria("ItalianoTest");
                 
        assertThat(materia_trovata.getNomeMateria()=="ItalianoTest");
    }
    
    @Test
    @Rollback(false)
    @Order(3)
    public void testUpdateMateria() {
    	Materia materia = repository.findByNomeMateria("ItalianoTest");
        materia.setNomeMateria("MatematicaTest");
        repository.save(materia);
        
    	Materia materia_modificata = repository.findByNomeMateria("MatematicaTest");
        assertThat(materia_modificata.getNomeMateria()=="MatematicaTest");
    }
    
    @Test
    @Order(4)
    public void testListMaterie() {
    	
        List<Materia> materie = repository.findAll();
        assertThat(materie).size().isGreaterThan(0);
    }
    
    @Test
    @Rollback(false)
    @Order(5)
    public void testDeleteMateria() {
    	Materia materia = repository.findByNomeMateria("MatematicaTest");
        repository.deleteMateriaByIdMateria(materia.getIdMateria());
         
        Materia deletedMateria = repository.findByNomeMateria("1Y");
         
        assertThat(deletedMateria).isNull();       
         
    }
}
