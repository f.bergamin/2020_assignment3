package com.assignment3.ClassRegister.test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.junit.jupiter.api.Order;
import org.springframework.test.annotation.Rollback;
import org.junit.jupiter.api.MethodOrderer;

import com.assignment3.ClassRegister.Entity.Persona;
import com.assignment3.ClassRegister.Entity.Professore;
import com.assignment3.ClassRegister.Repository.PersonaRepository;



@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class PersonaRepositoryTest {

    @Autowired
    private PersonaRepository repository;
     
    @Test
    @Rollback(false)
    @Order(1)
    public void testSaveNewPersona() {
    	Persona p = new Persona();
    	p.setNome("Mario");
    	p.setCognome("Rossi");
    	p.setEmail("mario.rossi@gmail.com");
    	p.setPassword("12345678");
    	p.setCodiceFiscale("abcdef12g34h567i");
        Persona persona_salvata = repository.save(p);
                 
        assertThat(persona_salvata.getId()).isGreaterThan(0);
    }
    
    
    @Test
    @Order(2)
    public void testFindPersona() {
    	Persona persona_trovata = repository.findByEmail("mario.rossi@gmail.com");
                 
        assertThat(persona_trovata.getEmail()=="mario.rossi@gmail.com");
    }
    
    @Test
    @Rollback(false)
    @Order(3)
    public void testUpdatePersona() {
    	Persona persona = repository.findByEmail("mario.rossi@gmail.com");
        persona.setRuolo("Professore");
        repository.save(persona);
        
    	Persona persona_modificata = repository.findByEmail("mario.rossi@gmail.com");
        assertThat(persona_modificata.getRuolo()=="Professore");
    }
    
    @Test
    @Order(4)
    public void testListProfessori() {
    	
        List<Professore> professori = repository.findAllProfessors();
        assertThat(professori).size().isGreaterThan(0);
    }
    
    @Test
    @Rollback(false)
    @Order(5)
    public void testDeleteProduct() {
    	Persona persona = repository.findByEmail("mario.rossi@gmail.com");
        repository.deleteById(persona.getId());
         
        Persona deletedPersona = repository.findByEmail("mario.rossi@gmail.com");
         
        assertThat(deletedPersona).isNull();       
         
    }
}
