# ASSIGNMENT 3
Link al repository: https://gitlab.com/f.bergamin/2020_assignment3

Federico Bergamin 749676    
Chiara Gabrieli 858664
<br>Alessio Meroni 797784

La documentazione è presente nel file **BergaminGabrieliMeroni.pdf**.

# Istruzioni per l'esecuzione
Per prima cosa, è necessario che un server SQL sia in esecuzione e che sia creato un DB denominato 'register'.<br>
Le credenziali di accesso al DB sono **user:**root **psw:**root. <br>
Per poter modificare i dati `e possibile accedere al file _application.properties_ e settare le credenziali a voi gradite.<br>
Successivamente si prosegue eseguendo le seguenti righe di comando nel Prompt dei comandi:
- `git clone https://gitlab.com/f.bergamin/2020_assignment3.git`
- `cd 2020_assignment3\ClassRegister`
- `mvn spring-boot:run`

L’applicazione sarà in esecuzione in `localhost:8080`

Per eseguire i test si può utilizzare la riga di comando:
- `mvn test`
